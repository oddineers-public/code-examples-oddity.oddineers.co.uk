#!/usr/bin/env python3

import requests
import json

class Matterhook:
    """
    A simple class for posting messages to Mattermost or Slack via incoming webhooks.
    """

    def __init__(self):
        self.status_code = None
        self.headers = {'content-type': 'application/json'}

    def post_message(self, url, message_payload, headers=None, timeout=None):
        """
        POST a message to an incoming webhook URL with a given payload and headers.

        :param url: The URL of the incoming webhook.
        :type url: str
        :param message_payload: The message payload to send to the webhook.
        :type message_payload: dict
        :param headers: Additional headers to send with the request.
        :type headers: dict or None
        :param timeout: Timeout in seconds for the request.
        :type timeout: float or None
        :raises ValueError: If the URL or message payload is invalid.
        :raises requests.exceptions.RequestException: If there was an error sending the request.
        :return: The response object if the request was successful.
        :rtype: requests.Response
        """
        if not message_payload:
            raise ValueError("Empty message payload provided.")
        if not isinstance(message_payload, dict):
            raise ValueError("Message payload should be a dictionary.")

        if not url:
            raise ValueError("Empty URL provided.")
        if not isinstance(url, str):
            raise ValueError("URL should be a string.")

        if headers is not None and not isinstance(headers, dict):
            raise ValueError("Headers should be a dictionary.")

        self.headers.update(headers or {})

        try:
            response = requests.post(url, data=json.dumps(message_payload), headers=self.headers, timeout=timeout)
            response.raise_for_status()
            self.status_code = response.status_code
            return response
        except requests.exceptions.RequestException as e:
            raise e

    def get_status_code(self):
        """
        Return the status code of the last request, or None if no request has been sent.

        :return: The HTTP status code of the last request.
        :rtype: int or None
        """
        return self.status_code


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    # Adds optional arguments to the script to set the webhook url, message and username
    # All arguments are optional as we will handle required args in our basic class.
    parser.add_argument('-w', '--webhook', dest='webhook', help='Incoming Webhook address.', type=str)
    parser.add_argument('-m', '--message', dest='message', help='Body of message.', type=str)
    parser.add_argument('-u', '--username', dest='username', help='Username to post as.', type=str)
    args = parser.parse_args()

    payload = {
        'text': args.message
    }

    # If username is provided
    if args.username:
        payload['username'] = args.username

    # Init class to post message
    notify = Matterhook()
    notify.post_message(args.webhook, payload)

    # Print help if response was not successful; not 20x
    if notify.get_status_code() not in [200, 201]:
        parser.print_help()
