#requires -version 2
<#
.SYNOPSIS
  Post messages to Mattermost/Slack
.DESCRIPTION
  Demonstrates a simple was to Post messages to Mattermost/Slack either directly through running the script or as a scheduled task.
.PARAMETER EventType
  Describe the current event.
.PARAMETER WebHook
  Provide the webhook url to deliver the message payload to.
.INPUTS
  <Inputs if any, otherwise state None>
.OUTPUTS
  None.
.NOTES
  Version:        1.0
  Author:         Steven
  Creation Date:  01/10/2019
  Purpose/Change: Demonstration script development
  
.EXAMPLE
  Run directly: .\\logon.ps1 -EventType "Run type description here." -WebHook "https://<webhook_url>/hooks/xxxxxxxxxxxxxxxxx"
  Run as a Scheduled task action:
  Program: powershell
  Arguments: -file .\\logon.ps1 -EventType "Run type description here." -WebHook "https://<webhook_url>/hooks/xxxxxxxxxxxxxxxxx"
  Start in: C:\example\
#>


Param(
    [parameter(
        Mandatory=$true,
        HelpMessage="Event description required."
    )]
    [string]$EventType,
    [parameter(
        Mandatory=$true,
        HelpMessage="Mattermost/Slack webhook URL required."
    )]
    [string]$WebHook
)


function Post-Mattermost {
    <#
    .Synopsis
       Post text to a Mattermost Channel via Webhooks
    .DESCRIPTION
       Long description
    .EXAMPLE
       Post-Mattermost -uri "https://<webhook_url>/hooks/xxxxxxxxxxxxxxxxx" -text "New message from Powershell" -user "Anomandaris"
    .INPUTS
       $uri,$text,$user
    .NOTES
       Webhook url and message body are mandatory, username is optional.
    #>
    [CmdletBinding()]Param
    (
        # Incoming Webhook
        [Parameter(Mandatory=$true
        )]
        [string]$uri,
        # Body of message
        [Parameter(Mandatory=$true
        )]
        [string]$text,
        # Username to post as
        [Parameter(Mandatory=$false
        )]
        [string]$user
     )
    $Payload = @{ 
        text=$text; 
        username=$user;
    }
    Invoke-RestMethod -Uri $uri -Method Post -ContentType 'application/json' -Body (ConvertTo-Json $Payload)
}

# Build up some message details relating to the current logged in user.
$levent = "Event details:"
$user = "User: **$env:UserName**" 
$machine = "[$env:UserDomain\$env:ComputerName]"
$datenow = Get-Date
$datenow = $datenow.tostring("**HH:mm:ss** on dd/MM/yyyy")
$action = $EventType
$msg = "$levent `n$user triggered: **$action** at $datenow `nFrom: $machine "

# Send the message
Post-Mattermost -uri $WebHook -text $msg
